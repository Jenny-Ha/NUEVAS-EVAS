<?php
/**
 * The Template for displaying all single posts.
 *
 */

global $apollo13;
get_header(); ?>

<?php the_post(); ?>

<?php a13_title_bar(); ?>

<article id="content" class="clearfix">

    <?php a13_header_tools() ?>

    <div id="col-mask">

        <div id="post-<?php the_ID(); ?>" <?php post_class('post-content'); ?>>
            <?php
                echo '<h2 class="post-title">'.get_the_title().'</h2>';
                a13_post_meta();
                a13_top_image_video();
            ?>

            <div class="real-content">
                <?php the_content(); ?>

                <div class="clear"></div>

                <?php
                    wp_link_pages( array(
                        'before' => '<div id="page-links">'.__fe('Pages: '),
                        'after'  => '</div>')
                    );
                ?>
            </div>

           
            <?php a13_similar_posts(); ?>

            <?php comments_template( '', true ); ?>
        </div>



        <?php get_sidebar(); ?>

    </div>

</article>

<?php get_footer(); ?>
