<!DOCTYPE html>
<!--[if lt IE 8]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie10 lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie10" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="icon" type="image/png" href="/wp-content/themes/NUEVAS-EVAS/img/favicon.png" />


<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.min.js" type="text/javascript"></script>

<?php
    global $apollo13;
    a13_theme_head();
    wp_head();
?>
    <!-- Temporal links TODO: Arreglar en funciones php -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>\css\icomoon-fonts.css">
</head>

<body id="top" <?php body_class(); ?>>
    <!-- Funciones PHP para header-->
    <?php
        $fixed_header   = $apollo13->get_option( 'appearance', 'fixed_header' ) === 'on';
        $header_classes  = $fixed_header ? 'sticky ': '';
    ?>
    <!-- Fin Funciones PHP -->

    <header id="header" class="<?php echo esc_attr($header_classes); ?>">
        <div class="top-bar-container gray">
            <div class="top-bar">
                <?php get_user_top_menu(); ?>
		<div class="socials">
                    <a target="_blank" href="http://www.facebook.com/nuevasevas" title="Síguenos en Facebook"><i class="icon-facebook"></i></a>
                    <a target="_blank" href="http://www.twitter.com/nuevasevas" title="Siguenos en Twitter" ><i class="icon-twitter"></i></a>
                    <a target="_blank" href="http://www.instagram.com/nuevasevas_rawveganperu" title="Síguenos en Instagram"><i class="icon-instagram"></i></a>
                    <a target="_blank" href="#" title="Siguenos en Snapchat"><i class="icon-snapchat-ghost"></i></a>
                </div>
                   
            </div>
        </div>
        <div class="head clearfix">
            <div class="logo-container">
                <a id="logo" href="/" rel="home" target="_blank">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png"  alt="Testing Projects">
                </a>
            </div>

            <nav id="access" role="navigation" class="<?php echo esc_attr($access_classes); ?>">
                <h3 class="assistive-text"><?php _fe( 'Main menu' ); ?></h3>
                <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff. */ ?>
                <a class="assistive-text" href="#begin-of-content" title="<?php esc_attr_e( __fe('Skip to primary content') ); ?>"><?php _fe( 'Skip to primary content' ); ?></a>
                <a class="assistive-text" href="#secondary" title="<?php esc_attr_e( __fe('Skip to secondary content') ); ?>"><?php _fe( 'Skip to secondary content' ); ?></a>

                <div class="menu-container clearfix">
			<?php display_menu('Conócenos','menu_conocenos');?>
			<?php display_menu('Recursos','menu_recursos');?>
			<?php display_menu('Nuestros Productos','menu_productos');?>
                </div>

            </nav><!-- #access -->
        </div>
    </header>

    <div id="mid" class="clearfix<?php echo a13_get_mid_classes(); ?>">

        <?php
            if( 0 && WP_DEBUG ){ $apollo13->page_type_debug(); }
