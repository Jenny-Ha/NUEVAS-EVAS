<?php global $apollo13; ?>
		</div><!-- #mid -->


        <footer id="footer" class="glass">
            <div class="foot-widget-content">
                <div class="foot-widgets clearfix four-col">
                    <div id="question" class="footer-widget widget widget_text">
                        <h3><span class="font-20">Pregúntanos</span></h3>
                            <div class="textwidget">
                                <p>Aqui puedes hacer cualquier pregunta de Salud, Belleza y Vida . Nuestros especialistas te responderán <br>
                                <a href="/dwqa-questions" class="" target="_blank">Ver Peguntas</a>
                                </p>
                                <form>
                                    <!--<p><input type="text" placeholder="Haz una pregunta"></p>-->
                                    <textarea cols="30" rows="5"  placeholder="Haz una pregunta"></textarea>
                                    <p class="text-right"><input type="submit" value="ENVIAR"></p>
                                </form>
                                
                            </div>
                    </div>
                    <div id="recent-posts-3" class="footer-widget widget widget_recent_posts widget_about_posts">
                        <h3><span class="font-20">Entradas Recientes</span></h3>

<?php query_posts('showposts=3');?>
<?php while (have_posts()) : the_post(); ?>
 <div class="item clearfix full">
                            <a class="thumb" href="<?php echo the_permalink() ?>" title="<?php the_title(); ?>">
                        <img width="100" height="75" src="<?php echo the_post_thumbnail() ?>" class="attac
hment-sidebar-size size-sidebar-size wp-post-image" alt="Post3" srcset="<?php echo the_post_thumbnail() ?> 100w, <?php echo the_post_thumbnail() ?> 504w, <?php echo the_post_thumbnail() ?> 408w" sizes="(max-width: 100px) 100vw, 100px"></a>
                            <a class="post-title" href="<?php echo the_permalink() ?>" title="<?php the_title(); ?>">
                                <?php the_title(); ?>
                            </a>
                            <time class="entry-date" datetime="2013-11-26T09:25:48+00:00"><?php echo the_time('F j, Y') ?></time>
                        </div>
<?php endwhile; ?>
                    </div>
                    <hr class="foot-separator after_second">

                    <div id="archives-3" class="footer-widget widget widget_archive">
                        <h3><span class="font-20">Más</span></h3>
                        <ul class="uppercase">
                            <li><a href="/contacto">Contacto</a></li>
                            <li><a href="/talleres">Talleres</a></li>
                            <li><a href="/charlas">Charlas</a></li>
                            <li><a href="/eventos">Eventos</a></li>
                            <li><a href="/prensa">Prensa</a></li>
                            <!-- <li><a href="#">Afíliate</a></li> -->
                        </ul>
                    </div>
                    <hr class="foot-separator after_third">

                    <div id="a13-social-icons-2" class="footer-widget widget widget_a13_social_icons">
                        <h3 ><span class="font-20">Síguenos</span></h3>
                        <div class="socials">
                            <a target="_blank" href="http://www.facebook.com/nuevasevas" title="Síguenos en Facebook"><i class="icon-facebook"></i></a>
                            <a target="_blank" href="http://www.twitter.com/nuevasevas" title="Siguenos en Twitter" ><i class="icon-twitter"></i></a>
                            <a target="_blank" href="http://www.instagram.com/nuevasevas_rawveganperu" title="Síguenos en Instagram"><i class="icon-instagram"></i></a>
                            <a target="_blank" href="#" title="Siguenos en Snapchat"><i class="icon-snapchat-ghost"></i></a>
                        </div>
                    </div>
                    <hr class="foot-separator end_list">
                </div>
                <div class="footer-items clearfix">
                    <div class="copyright">Nuevas Evas © 2016 Arequipa / Peru</div>
                    <div class="text-right"> Powered by 
                        <a href="http://atixplus.com/" class="logo-atix" title="Equipo Atix Plus" target="_blank"></a>
                    </div>    
                    
                    <a href="#top" id="to-top" class="to-top fa fa-chevron-up" style="opacity: 1;"></a>
                </div>
            </div>
        </footer>
<?php
        /* Always have wp_footer() just before the closing </body>
         * tag of your theme, or you will break many plugins, which
         * generally use this hook to reference JavaScript files.
         */

        wp_footer();
?>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?4AOq00DraDqK6O7x0AgzdWSF8iSY9H27";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
</body>
</html>
